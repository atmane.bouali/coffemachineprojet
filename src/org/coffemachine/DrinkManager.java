package org.coffemachine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class DrinkManager {
	
	public Map<String, Drink> drinkList = new HashMap<String, Drink>();
	
	public void addDrinks(Drink... drinks){
		System.out.println("Ajout des boissons disponible en cours.");
		for(Drink drink : drinks)
			this.drinkList.put(drink.getName(), drink);
		System.out.println("Ajout des boissons disponible terminé.");
	}
	
	public Drink getChoosenDrink(String drinkName){
		System.out.println("Recuperation de la boisson choisie par le client ("+drinkName+")");
		return this.drinkList.get(drinkName);
	}
	
	public String generateMessageProtocol(Drink drink){
		System.out.println("Generation du message-protocol pour préparer la boisson : "+drink.getName());
		return drink.toMessageProtocol();
	}
	
	public String generateMessageProtocol(String msgToDisplay){
		System.out.println("Generation du message-protocol pour le message : "+msgToDisplay);
		return "M:"+msgToDisplay;
	}
	
	
	public boolean sendMessageProtocol(String msgProtocol){
		System.out.println("Simulation d'envoi de la commande au COFFEE-MAKER...");
		return true;
	}
}
