package org.coffemachine;

public class Drink {
	String name;
	int qtteSogar;
	char code;
	
	public Drink(String name, int qtteSogar, char code) {
		this.name = name;
		this.qtteSogar = qtteSogar;
		this.code = code;
	}
	
	public Drink(String name, char code) {
		this.name = name;
		this.qtteSogar = 0;
		this.code = code;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQtteSogar() {
		return qtteSogar;
	}

	public void setQtteSogar(int qtteSogar) {
		this.qtteSogar = qtteSogar;
	}
	
	public char getCode() {
		return code;
	}

	public void setCode(char code) {
		this.code = code;
	}

	public String toMessageProtocol() {
		if( this.qtteSogar == 0)
			return this.code+"::";
		return this.code+":"+this.qtteSogar+":0";
	}
}
