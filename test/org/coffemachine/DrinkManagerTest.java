package org.coffemachine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DrinkManagerTest {
	DrinkManager drinkManager = new DrinkManager();
	Drink t = new Drink("TEA", 1, 'T');
	Drink h = new Drink("CHCOLATE", 'H');
	Drink c = new Drink("COFFEE", 2, 'C');
	String m = "MESSAGE...";
	
	
	@BeforeEach
	void setUp() throws Exception {
		
	}

	@Test
	void addDrinksTest() {
		drinkManager.addDrinks(t,h,c);	
		assertEquals(3, drinkManager.drinkList.size(), "TEST : Ajout des boisson disponibles");
	}
	
	@Test
	void getChoosenDrinkTest() {
		drinkManager.addDrinks(t,h,c);
		Drink selectedDrink = drinkManager.getChoosenDrink("COFFEE");
		assertEquals(c, selectedDrink, "TEST : choix de la boisson");
	}
	
	@Test
	void generateMessageProtocolWithSogarTest() {
		String generatedMsgProtocol = drinkManager.generateMessageProtocol(c);
		assertEquals("C:2:0", generatedMsgProtocol, "TEST : generer le message-protocol pour un café avec 2 sucre");
	}
	
	@Test
	void generateMessageProtocolWithoutSogarTest() {
		String generatedMsgProtocol = drinkManager.generateMessageProtocol(h);
		assertEquals("H::", generatedMsgProtocol, "TEST : generer le message-protocol pour un chocolat sans sucre");
	}
	
	@Test
	void generateMessageProtocolForMessageTest() {
		String generatedMsgProtocol = drinkManager.generateMessageProtocol("Merci pour votre commande");
		assertEquals("M:Merci pour votre commande", generatedMsgProtocol, "TEST : generer le message-protocol pour l'affichage d'un message");
	}
	
	@Test
	void sendMessageProtocolTest() {
		boolean responseCoffeeMaker = drinkManager.sendMessageProtocol("T:1:0");
		assertEquals(true, responseCoffeeMaker, "TEST : Envoi de la commannde au CoffeeMaker");
	}
}
